package com.vw.datastructure.arrayandstring.hashtable;


import com.vw.Application;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {Application.class})
public class Hashtable {

    Node<Integer, String> n1 = new Node(1, "Spring"),
            n2 = new Node(2, "Summer"),
            n3 = new Node(3, "Fall"),
            n4 = new Node(4, "Winter"),
            n5 = new Node(3, "Autumn");

    MyHashTable<Integer, String> ht = new MyHashTable<>(4);



    @Test
    public void testHashtable1(){

        ht.put(1, "Spring");
        assertThat(ht.get(1).get()).isEqualTo("Spring");

    }

    @Test
    public void testHashtable2(){

        ht.put(1, "Spring");
        ht.put(2, "Summer");
        ht.display();

        assertThat(ht.get(1).get()).isEqualTo("Spring");
        assertThat(ht.get(2).get()).isEqualTo("Summer");

    }

    @Test
    public void testHashtable3(){

        ht.put(1, "Spring");
        ht.put(2, "Summer");
        ht.put(3, "Fall");
        ht.display();

        assertThat(ht.get(1).get()).isEqualTo("Spring");
        assertThat(ht.get(2).get()).isEqualTo("Summer");
        assertThat(ht.get(3).get()).isEqualTo("Fall");

    }

    @Test
    public void testHashtable4(){

        ht.put(1, "Spring");
        ht.put(2, "Summer");
        ht.put(3, "Fall");
        ht.put(4, "Winter");
        ht.display();

        assertThat(ht.get(1).get()).isEqualTo("Spring");
        assertThat(ht.get(2).get()).isEqualTo("Summer");
        assertThat(ht.get(3).get()).isEqualTo("Fall");
        assertThat(ht.get(4).get()).isEqualTo("Winter");
    }

    @Test
    public void testHashtable5(){

        ht.put(1, "Spring");
        ht.put(2, "Summer");
        ht.put(3, "Fall");
        ht.put(4, "Winter");
        ht.put(5, "Autumn");
        ht.display();

        assertThat(ht.get(1).get()).isEqualTo("Spring");
        assertThat(ht.get(2).get()).isEqualTo("Summer");
        assertThat(ht.get(3).get()).isEqualTo("Fall");
        assertThat(ht.get(4).get()).isEqualTo("Winter");
        assertThat(ht.get(5).get()).isEqualTo("Autumn");
    }

    @Test
    public void testHashtable6(){

        ht.put(1, "Spring");
        ht.put(2, "Summer");
        ht.put(3, "Fall");
        ht.put(4, "Winter");
        ht.put(3, "Autumn");
        ht.display();

        assertThat(ht.get(1).get()).isEqualTo("Spring");
        assertThat(ht.get(2).get()).isEqualTo("Summer");
        assertThat(ht.get(3).get()).isEqualTo("Autumn");
        assertThat(ht.get(4).get()).isEqualTo("Winter");

    }

    @Test
    public void testHashtable7(){

        ht.put(1, "Spring");
        ht.put(2, "Summer");
        ht.put(3, "Fall");
        ht.put(4, "Winter");
        ht.put(3, "Autumn");

        ht.put(5, "East");
        ht.put(6, "South");
        ht.put(7, "West");
        ht.put(8, "North");

        ht.display();

        assertThat(ht.get(1).get()).isEqualTo("Spring");
        assertThat(ht.get(2).get()).isEqualTo("Summer");
        assertThat(ht.get(3).get()).isEqualTo("Autumn");
        assertThat(ht.get(4).get()).isEqualTo("Winter");
        assertThat(ht.get(5).get()).isEqualTo("East");
        assertThat(ht.get(6).get()).isEqualTo("South");
        assertThat(ht.get(7).get()).isEqualTo("West");
        assertThat(ht.get(8).get()).isEqualTo("North");

    }

    @Test
    public void indexTest () {
        assertThat(ht.keyIndex(5)).isEqualTo(1);
    }
}

import java.util.Formatter;

/**
 * Created by Vernon on 11/3/2016.
 */

interface I1 {
    String name = "Me";
    String s1= "You";
}

interface I2 extends I1{
    String name = "Me too";
}
/*public*/ class C82 implements I2{

    public static void main(String[] args) {
         System.out.print(I2.name+",");
            System.out.print(I2.s1+",");
//           System.out.print(((I1)new C2()).name);

           }
}

class A {

    static String sb = "sfsf";

    A(){
        this.sb="sfsf";
    }
}

class P{

    static int count = 0;

    P(){ count++;}
static    int getCount(){return count;}

    protected int foo(){
return 1;
    }
}

public class C2 extends P{

    public C2(){
        count++;
    }

    @Override
    public int foo(){
        return 2;
    }

    public static void main(String [] args){

        System.out.println(getCount());
        C2 c = new C2();
        System.out.println(getCount());

        StringBuffer sb = new StringBuffer("C");
        Formatter f = new Formatter(sb);

        String sA = "A", sB= "B", sNull = null;
        f.format("%s%s", sA, sB);
        System.out.println(f);
        f.format("%-2s", sB);
        System.out.println(f);
        f.format("%b", sNull);
        System.out.println(f);
    }
        }
package com.vw.crackingTheCodinginterview.stairs;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Vernon on 11/13/2016.
 *
 * Davis has  staircases in his house and he likes to climb each staircase , , or  steps at a time. Being a very precocious child, he wonders how many ways there are to reach the top of the staircase.

 Given the respective heights for each of the  staircases in his house, find and print the number of ways he can climb each staircase on a new line.

 */
public class NumberOfWaysToTakeStairs {

    static Map<Integer, Integer> m = new HashMap<>();

    static int countIt(int n){

        if(!m.containsKey(n)){
            int c = countIt(n - 1) + countIt(n - 2) + countIt(n - 3);
            m.put(n, c);
        }
        return m.get(n);

    }
    public static void main(String[] args) {

        m.put(1,1);
        m.put(2,2);
        m.put(3,4);

        int[] s = {4, 7};
        for(int a0 = 0; a0 < s.length; a0++){
            int n = s[a0];
            System.out.println(countIt(n));
        }
    }
}

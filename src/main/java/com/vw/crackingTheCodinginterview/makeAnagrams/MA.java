package com.vw.crackingTheCodinginterview.makeAnagrams;

/**
 * Created by Vernon on 10/27/2016.
 *
 * Alice is taking a cryptography class and finding anagrams to be very useful. We consider two strings to be anagrams of each other if the first string's letters can be rearranged to form the second string. In other words, both strings must contain the same exact letters in the same exact frequency For example, bacdc and dcbac are anagrams, but bacdc and dcbad are not.

 Alice decides on an encryption scheme involving two large strings where encryption is dependent on the minimum number of character deletions required to make the two strings anagrams. Can you help her find this number?

 Given two strings,  and , that may or may not be of the same length, determine the minimum number of character deletions required to make  and  anagrams. Any characters can be deleted from either of the strings.

 This challenge is also available in the following translations:

 Chinese
 Russian
 Input Format

 The first line contains a single string, .
 The second line contains a single string, .

 Constraints

 It is guaranteed that  and  consist of lowercase English alphabetic letters (i.e.,  through ).
 */
public class MA {

    public static int numberNeeded(String first, String second) {
        int[] m = new int[26];
        for(int i=0; i<26;i++){
            m[i] = 0;
        }
    char a = 'a';
//        char [] ca = new char[first.length()];
        char[] charArray= first.toCharArray();
        for(int i = 0; i< charArray.length;i++){
//            System.out.println(first.charAt(i) + " "i + (first.charAt(i) -'a'));
            m[charArray[i] - a]++;
        }
        charArray = second.toCharArray();
        for(int i=0;i<charArray.length;i++){
//            System.out.println(second.charAt(i) + " " + (second.charAt(i) -'a'));
            m[charArray[i] - a]--;
        }
        int count = 0;
        for(int i=0;i<26;i++){
            System.out.print(i + ":" + Integer.valueOf(m[i]) + ", ");
//            if(m[i] != 0)
                count+=Math.abs(m[i]);
        }
        System.out.println("");
        return count;

    }

    static public void main(String[] args){

        System.out.println(numberNeeded("dece", "abcac"));
    }
}

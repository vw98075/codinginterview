package com.vw.crackingTheCodinginterview.mergeSort;

/**
 * Created by Vernon on 11/12/2016.
 *
 * http://www.geeksforgeeks.org/counting-inversions/
 *
 * the following isn't correct for some reason
 */
public class NumberOfSwap {

    // Merges two subarrays of arr[].
    // First subarray is arr[l..m]
    // Second subarray is arr[m+1..r]
    static int merge(int arr[], int l, int m, int r){

        int num = 0;
        // Find sizes of two subarrays to be merged
        int n1 = m - l + 1;
        int n2 = r - m;

        /* Create temp arrays */
        int la[] = new int [n1];
        int ra[] = new int [n2];

        /*Copy data to temp arrays*/
        for (int i=0; i<n1; ++i)
            la[i] = arr[l + i];
        for (int j=0; j<n2; ++j)
            ra[j] = arr[m + 1+ j];

        int i=0, j=0, k = l;
        while(i <n1 && j <n2){
            if(la[i] > ra[j]){
                num += m - i + 1;
                arr[k++] = ra[j++] ;
            }else{
                arr[k++] = la[i++];
            }
        }

        while(i < n1){
            arr[k++] = la[i++];
        }

        while(j < n2){
            arr[k++] = ra[j++];
        }
        return num;
    }

    static int sort(int arr[], int l, int r){
        int totalNum=0;
        if (l < r)
        {
            // Find the middle point
            int m = (l+r)/2;

            // Sort first and second halves
            totalNum = sort(arr, l, m);
            totalNum += sort(arr , m+1, r);

            // Merge the sorted halves
            totalNum +=merge(arr, l, m, r);
        }
        return totalNum;
    }
    public static void main(String[] args){

        int[] a = {2, 1, 3, 1, 2};
        NumberOfSwap s = new NumberOfSwap();
        System.out.println(s.sort(a, 0, a.length - 1));
    }
}

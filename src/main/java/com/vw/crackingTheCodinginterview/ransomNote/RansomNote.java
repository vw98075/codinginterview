package com.vw.crackingTheCodinginterview.ransomNote;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Vernon on 11/10/2016.
 *
 * A kidnapper wrote a ransom note but is worried it will be traced back to him. He found a magazine and wants to know if he can cut out whole words from it and use them to create an untraceable replica of his ransom note. The words in his note are case-sensitive and he must use whole words available in the magazine, meaning he cannot use substrings or concatenation to create the words he needs.

 Given the words in the magazine and the words in the ransom note, print Yes if he can replicate his ransom note exactly using whole words from the magazine; otherwise, print No.
 */
public class RansomNote {


    static String canIt(String[] wm, String[] wr){


        Map<String, Integer> map = new HashMap<>();
        for(int i=0; i< wm.length; i++){
            Integer v = map.get(wm[i]);
            if(v == null){
                map.put(wm[i], 1);
            }else
                map.put(wm[i], v+1);
        }
        for(int i=0; i<wr.length; i++){
            Integer v = map.get(wr[i]);
            if(v == null)
                return "No";
            if(v == 1){
                map.remove(wr[i]);
            }else{
                map.put(wr[i], v - 1);
            }
        }
        return "Yes";

    }
}

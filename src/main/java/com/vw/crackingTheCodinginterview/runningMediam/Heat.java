package com.vw.crackingTheCodinginterview.runningMediam;

import java.util.Arrays;

/**
 * Created by Vernon on 11/11/2016.
 */
public class Heat {

    static double doIt(int[] a) throws Exception {
        if(a.length == 0)
            throw new Exception("Input error");
        if(a.length ==1)
            return (double)a[0];

        Arrays.sort(a);

        if(a.length % 2 != 0)
            return (double)a[(a.length - 1)/2];

        return (double)((a[(a.length - 1)/2] + a[a.length/2])/2.0);


    }

    static void doAll(int[] a){
        try{
            for(int i=1; i<=a.length; i++){
                System.out.println(doIt(Arrays.copyOfRange(a, 0, i)));
            }
        }catch(Exception e){

        }
    }
}

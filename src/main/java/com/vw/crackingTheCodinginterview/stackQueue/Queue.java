package com.vw.crackingTheCodinginterview.stackQueue;

import java.util.Stack;

/**
 * Created by Vernon on 11/2/2016.
 */
public class Queue<T> {
    private Stack<T> stack1 = new Stack<>();
    private Stack<T> stack2 = new Stack<>();


    public void enqueue(T n){
        stack1.push(n);
    }

    public T dequeue(){
        if(stack2.isEmpty())
            while(!stack1.isEmpty())
                stack2.push(stack1.pop());
        T tmp = null;
        if(!stack2.isEmpty())
            tmp = stack2.pop();
        return tmp;

    }

    public T peek(){
        if(stack2.isEmpty())
            while(!stack1.isEmpty())
                stack2.push(stack1.pop());
        T tmp = null;
        if(!stack2.isEmpty())
            tmp = stack2.peek();
        return tmp;
    }
}

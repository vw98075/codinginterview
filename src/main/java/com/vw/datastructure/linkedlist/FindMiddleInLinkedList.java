package com.vw.datastructure.linkedlist;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Vernon on 6/25/2016.
 */
public class FindMiddleInLinkedList{


    static public void main(String [] args) {

        List<String> l = new LinkedList<>();
        l.add("a");
        l.add("b");
        l.add("c");
        l.add("d");
        l.add("e");
        l.add("f");
        l.add("g");

        int running = 0;
        int m = 0;

        while (running < l.size()) {
            running += 2;
            m++;
        }

        System.out.println(m);
    }
}


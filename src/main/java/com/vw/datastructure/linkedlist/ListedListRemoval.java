package com.vw.datastructure.linkedlist;

/**
 * Created by Vernon on 11/21/2016.
 */
public class ListedListRemoval {

    static Node removeDuplicates(Node head) {
        //Write your code here
        if(head == null || head.next == null)
            return head;

        Node p = head, runner = p.next;
        while(runner != null){
            if(p.data == runner.data){
                runner = runner.next;
            }
            p.next = runner;
            p = runner;
            runner = runner.next;
        }
        return head;
    }

    Node insert(Node h, int data){
        Node p = new Node(data);
        if(h == null){
            h = p;
        }else if(h.next == null){
            h.next = p;
        }else {
            Node start = h;

        }
        return p;
    }

    public static void main(String[] args){


    }
}

class Node{
    int data;
    Node next;

    Node(int d){
        data = d;
        next = null;
    }

}
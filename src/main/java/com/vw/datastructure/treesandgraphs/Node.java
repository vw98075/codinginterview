package com.vw.datastructure.treesandgraphs;

/**
 * Created by Vernon on 6/25/2016.
 */
class Node {

    int key;
    Node l;
    Node r;

    Node(int v) {
        this.key = v;
    }

    void addLeftNode(Node l) {
        this.l = l;
    }

    void addRightNode(Node r) {
        this.r = r;
    }

    @Override
    public String toString() {
        return "value = " + key;
    }
}

package com.vw.datastructure.treesandgraphs;


/**
 * Created by Vernon on 6/25/2016.
 */
public class BinaryTree {

    Node root;

    BinaryTree(Node root){
     this.root = root;
    }

    BinaryTree(){
        this.root = buildTree();
    }

    Node buildTree(){
        Node root = new Node(1);
        Node b = new Node(2);
        Node c = new Node(3);
        Node e = new Node(4);
        Node f = new Node(5);

        b.addLeftNode(e);
        c.addLeftNode(f);

        root.addLeftNode(b);
        root.addRightNode(c);

        return root;
    }

    /* Given a binary tree, print its nodes according to the
        "bottom-up" postorder traversal. */
    void printPostorder(Node node)
    {
        if (node == null)
            return;

        // first recur on left subtree
        printPostorder(node.l);

        // then recur on right subtree
        printPostorder(node.r);

        // now deal with the node
        System.out.print(node.key + " ");
    }

    /* Given a binary tree, print its nodes in inorder*/
    void printInorder(Node node)
    {
        if (node == null)
            return;
 
        /* first recur on left child */
        printInorder(node.l);
 
        /* then print the data of node */
        System.out.print(node.key + " ");
 
        /* now recur on right child */
        printInorder(node.r);
    }

    /* Given a binary tree, print its nodes in preorder*/
    void printPreorder(Node node)
    {
        if (node == null)
            return;
 
        /* first print data of node */
        System.out.print(node.key + " ");
 
        /* then recur on left sutree */
        printPreorder(node.l);
 
        /* now recur on right subtree */
        printPreorder(node.r);
    }


    public void rotate(Node n){

        Node t;
//        if(n.linarl != null)
        t = n.l;

        n.l = n.r;
        n.r = t;

        if(n.l != null)
            rotate(n.l);
        if(n.r != null)
            rotate(n.r);
    }

    // Wrappers over above recursive functions
    void printPostorder()  {     printPostorder(root);  System.out.println(""); }
    void printInorder()    {     printInorder(root);   System.out.println(""); }
    void printPreorder()   {     printPreorder(root);  System.out.println(""); }

    void ratateFromTop(){
        rotate(this.root);
    }


}

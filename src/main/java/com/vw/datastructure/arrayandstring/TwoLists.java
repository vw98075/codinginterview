package com.vw.datastructure.arrayandstring;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Created by Vernon on 6/19/2016.
 */
public class TwoLists {

  static Character[] removeDuplicationsInSortedArray(Character[] a){

      Arrays.sort(a);
      System.out.println(Arrays.toString(a));
      int i = 0, k = 1;
      while(i< a.length - 1 && k+i < a.length){

          while(a[i] == a[i + k]){
              ++k;
          }
          a[i+1] = a[i+k];
          i++;
      }
      while(i<a.length){
          a[i++] = null;
      }
      return a;
  }

  static  Character[] mergeTwoSortedLists(Character[] a, Character[] b){

        int m=0, n= 0, k = 0;

        Arrays.sort(a);
        Arrays.sort(b);
      System.out.println(Arrays.toString(a));
      System.out.println(Arrays.toString(b));
        Character[] d = new Character[a.length + b.length];

          d[k++] = (a[n] <= b[m]) ? a[n++] : b[m++];

        while(n < a.length && m < b.length) {
            if(a[n] < b[m]){
                System.out.println("2: " + a[n]);
                if(d[k-1] != a[n])
                d[k++] = a[n++];
                else
                    n++;
            } else if(a[n] > b[m]) {
                System.out.println("3: " + b[m]);
                if(d[k-1] != b[m])
                d[k++] = b[m++];
                else
                    m++;
            }else{

                System.out.println("4: " + a[n] + " " +  b[m]);
                n++;
//                m++;
            }
        }
System.out.println("after");
        while(n < a.length) {
            d[k++] = a[n++];
        }
        while(m < b.length) {
            d[k++] = b[m++];
        }
        return d;
    }

    public static void main(String[] args){

        Character[] l1 = {'w', 'y', 'a', 'c', 'g', 'j'};
        Character[] l2 = {'q', 'h', 'i', 'b', 'a', 'c', 'e', 'r'};

        System.out.println(Arrays.toString(mergeTwoSortedLists(l1, l2)));

        Character[] l3 = {'w', 'y', 'a', 'c', 'g', 'j','q', 'h', 'i', 'b', 'a', 'c', 'e', 'r'};
        System.out.println(Arrays.toString(removeDuplicationsInSortedArray(l3)));
    }
}


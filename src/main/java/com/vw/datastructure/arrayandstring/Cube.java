package com.vw.datastructure.arrayandstring;

import java.util.*;

/**
 * Created by Vernon on 11/10/2016.
    a^3 + b^3 = c^3 d^3 where 0< a, b, c, d < 1000 they are integers
 */
public class Cube {

    class Data{

        int a, b, c, d;

        Data(int a, int b, int c, int d){
            this.a = a;
            this.b = b;
            this.c = c;
            this.d = d;
        }

        @Override
        public String toString(){
            return "( " + a + ", " + b + ", " + c + ", " + d + ")";
        }
    }

    class Pair{
        int a, b;

        Pair(int a, int b){
            this.a=a;
            this.b=b;
        }
    }

    // O(n^2)
    Set<Data> findWithHashMap(){

        Map<Integer, Pair> map = new HashMap<>();
        for(int i=1; i<10;i++){
            for(int j=1; j<10; j++){
                map.put((int)(Math.pow(i, 3) + Math.pow(j, 3)), new Pair(i, j));
            }
        }
        Set<Data> d = new HashSet<>();
        for(int i=1; i<10;i++){
            for(int j=1; j<10; j++){
                int v = (int)(Math.pow(i, 3) + Math.pow(j, 3));
                Pair p = map.get(v);
                if(p != null){
                    d.add(new Data(i, j, p.a, p.b));
//                    d.add(new Data(i, j, p.b, p.a));
                }
            }
        }

        return d;
    }


    // O(n^4)
     Set<Data> find(){
        Set<Data> d = new HashSet<>();
        for(int i=1; i<=1000;i++){
            for(int j=1;j<=1000; j++){
                for(int k=1;k<=1000;k++){
                    for(int l=1;l<=1000;l++){
                        if(Math.pow(i, 3) + Math.pow(j, 3) == Math.pow(k, 3) + Math.pow(l, 3)){
                            d.add(new Data(i, j, k, l)) ;
                        }
                    }
                }
            }
        }
        return d;
    }

    public static void main(String[] args){

        Cube c = new Cube();
        Set<Data> d = c.findWithHashMap();// c.find();
        Iterator<Data> itr = d.iterator();
        while(itr.hasNext()){
            System.out.println(itr.next().toString());
        }
    }
}

package com.vw.datastructure.arrayandstring.hashtable;

public class Test {

    static public void main(String [] args) {

        Node<Integer, String> n1 = new Node(1, "Spring"),
                n2 = new Node(2, "Summer"),
                n3 = new Node(3, "Fall"),
                n4 = new Node(4, "Winder"),
                n5 = new Node(3, "Autum");

        MyHashTable<Integer, String> ht = new MyHashTable<>(4);

        ht.put(1, "Spring");
        ht.display();
        ht.put(2, "Summer");
        ht.display();
        ht.put(3, "Fall");
        ht.display();
        ht.put(4, "Winder");
        ht.display();
        ht.put(5, "Autum");
        ht.display();
    }
}

package com.vw.datastructure.arrayandstring.hashtable;

import java.util.*;

public class  MyHashTable<K, V> {

    private List<LinkedList<Node<K, V>>> nodes;

    private final int size;

    public MyHashTable(int s) {
        this.size = s;
        this.nodes = new ArrayList<LinkedList<Node<K, V>>>(this.size);
        for(int i = 0; i < size; i++) {
            this.nodes.add(new LinkedList<>());
        }
    }

//    public boolean contains(V v) {
//
//        for(int i = 0; i < this.size; i++) {
//
//            if(nodes.get(i).contains(v))
//                return true;
//        }
//        return false;
//    }

    public boolean containsKey(K k) {

        return this.nodes.get(keyIndex(k)) != null;
    }

    int keyIndex(K k) {

        return k.hashCode() % this.size;
    }

    public Optional<V> get(K k) {

        List<Node<K, V>> l = this.nodes.get(keyIndex(k));

        if(l == null)
            return Optional.empty();

        for(int i = 0; i< this.nodes.size(); i++) {
            if(l.get(i).getKey().equals(k))
                return Optional.of(l.get(i).getValue());
        }
        return Optional.empty();
    }

    public boolean put(K k, V v) {

        int idx = this.keyIndex(k);

        List<Node<K, V>> l = this.nodes.get(idx);

        if(l.isEmpty()) {
            return l.add(new Node(k, v));
        }

        for(int i = 0; i < l.size(); i++) {

            Node n = l.get(i);
            if(k.equals(n.getKey())) {
                //
                if(v.equals(n.getValue()))
                    return false;

                n.setValue(v);
                return true;
            }
        }

        return l.add(new Node(k, v));
    }


    public boolean remove(K k, V v) {

        int idx = this.keyIndex(k);

        List<Node<K, V>> l = this.nodes.get(idx);

        return l.remove(new Node(k, v));
    }

    public void display() {

        for(int i = 0; i < size; i++) {
            List<Node<K, V>> l = this.nodes.get(i);
            l.stream().forEach( n -> System.out.print(n.toString() + " "));
            System.out.println();
        }
    }
}


class Node<K, V> {

    private K key;

    private V value;

    public Node(K k, V v){
        this.key = k;
        this.value = v;
    }

    public K getKey() {
        return key;
    }

    public V getValue() {
        return value;
    }

    public void setValue(V value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Node)) return false;
        Node<?, ?> node = (Node<?, ?>) o;
        return Objects.equals(key, node.key) &&
                Objects.equals(value, node.value);
    }

    @Override
    public int hashCode() {

        return Objects.hash(key, value);
    }

    @Override
    public String toString() {
        return "Node{" +
                "key=" + key +
                ", value=" + value +
                '}';
    }
}

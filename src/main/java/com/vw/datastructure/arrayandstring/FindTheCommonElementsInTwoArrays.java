package com.vw.datastructure.arrayandstring;

import java.util.*;

/**
 * Created by Vernon on 11/10/2016.
 */
public class FindTheCommonElementsInTwoArrays {

    // O(M + N)
    static int[] findTheCommonOnesByUsingHaskTable(int[] a, int[] b){

        Set<Integer> set = new HashSet<>(a.length);
        for(int i=0;i<a.length; i++){
            set.add(a[i]);
        }
        int[] r = new int[Math.min(a.length, b.length)];
        int k = 0;
        for(int j=0;j<b.length;j++){
            if(set.contains(b[j]))
                r[k++] = b[j];
        }
        int[] result = Arrays.copyOfRange(r, 0, k);
        return result;
    }

    // O(M * N)
    static int[] findtheCommonOnes(int[] a, int[] b){

        int[] r = new int[Math.min(a.length, b.length)];
        int k = 0;
        for(int i=0; i<a.length; i++){
            for(int j=0;j<b.length;j++){
                if(a[i] == b[j])
                    r[k++] = a[i];
            }
        }
        int[] result = Arrays.copyOfRange(r, 0, k);
        return result;
    }


    public static void main(String [] args){

        int [] a = {1,5,12,3,-15,52}, b = {3,1,6,5,57,13,17};

        System.out.println(Arrays.toString(findtheCommonOnes(a, b)));
        System.out.println(Arrays.toString(findTheCommonOnesByUsingHaskTable(a, b)));
    }
}

package com.vw.datastructure.arrayandstring;

import java.util.Scanner;

/**
 * Created by Vernon on 2/14/2016.
 */
public class Solution {

    void rotation(int r, int [][] mt, int m, int n){

        int [] flatten = new int[m*n];
        for(int i=0; i< m; i++){

            int ptr=0;

            //top row, left to right
            for (int j=i; j<n-i; j++) {
                flatten[ptr++] = mt[i][j];
            }

            // right column, top to bottom
            for (int j=i+1; j<m-i; j++) {
                flatten[ptr++] = mt[j][n-1-i];
            }

            // bottom row, right to left
            for (int j=n-2-i; j>=i; j--) {
                flatten[ptr++] = mt[m-1-i][j];
            }

            // left column, bottom to top
            for (int j=m-2-i; j>i; j--) {
                flatten[ptr++] = mt[j][i];
            }


            int new_start_pos = r%ptr;
            //printf ("new_start_pos = %d ", new_start_pos);
            //printf ("\n\n");

            if (new_start_pos>0) {
                //top row, left to right
                for (int j=i; j<n-i; j++) {
                    mt[i][j] = flatten[new_start_pos];
                    new_start_pos = (new_start_pos+1) % ptr;
                }

                // right column, top to bottom
                for (int j=i+1; j<m-i; j++) {
                    mt[j][n-1-i] = flatten[new_start_pos];
                    new_start_pos = (new_start_pos+1) % ptr;
                }

                // bottom row, right to left
                for (int j=n-2-i; j>=i; j--) {
                    mt[m-1-i][j] = flatten[new_start_pos];
                    new_start_pos = (new_start_pos+1) % ptr;
                }

                // left column, bottom to top
                for (int j=m-2-i; j>i; j--) {
                    mt[j][i] = flatten[new_start_pos];
                    new_start_pos = (new_start_pos+1) % ptr;
                }
            }


        }


    }
    public void main(String [] args){

        Scanner sc = new Scanner(System.in);



    }


}

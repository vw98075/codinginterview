package com.vw.datastructure.arrayandstring;

        import java.util.*;

/**
 * Created by Vernon on 11/17/2016.
 */
public class Bestroute {

//    private static List<String> romanizeNames(List<String> namesInOrder) {
//        /* Write your code here */
//        Map<String, Integer> map = new HashMap<String, Integer>();
//        for(String n : namesInOrder){
//            Integer count = map.get(n);
//            if(count == null){
//                map.put(n, 1);
//            }else{
//                map.put(n, count + 1);
//            }
//        }
//
//        List<String> output = new ArrayList<String>(namesInOrder.size());
//        Set<String> repectedNames = new HashSet<String>();
//        Collections.reverse(namesInOrder);
//        for(String n : namesInOrder){
//            Integer count = map.get(n);
//            if(count == 1) {
//                if (repectedNames.contains(n)) {
//                    output.add(n + " (1)");
//                }else {
//                    output.add(n);
//                }
//
//            }else{
//                output.add(n + " (" + count +  ")");
//                repectedNames.add(n);
//            }
//            map.put(n, count - 1);
//        }
//        Collections.reverse(output);
//        return output;
//    }

    static long findBestRoute(int[][] matrix, int N) {

        int[][] costs = new int[N][N];

        costs[0][0] = matrix[0][0];
        for (int i = 1; i < N; i++) {
            costs[0][i] = matrix[0][i] + costs[0][i - 1];
        }

        for (int i = 1; i < N; i++) {
            costs[i][0] = matrix[i][0] + costs[i - 1][0];
        }

        for (int i = 1; i < N; i++) {
            for (int j = 1; j < N; j++) {
                costs[i][j] = matrix[i][j]
                        + Math.min(costs[i - 1][j], costs[i][j - 1]);
            }
        }
        return costs[N - 1][N - 1];

//calculate the solution for bottom and right
//        for (int i = gridSize - 2; i >= 0; i--) {
//            grid[gridSize - 1, i] += grid[gridSize - 1, i+1];
//            grid[i,gridSize - 1] += grid[i+1, gridSize - 1];
//        }
//
//        for (int i = gridSize - 2; i >= 0; i--) {
//            for (int j = gridSize - 2; j >= 0; j--) {
//                grid[i, j] += Math.Min(grid[i + 1, j], grid[i, j + 1]);
//            }
//        }
    }

    /* A utility function that returns minimum of 3 integers */
    private static int min(int x, int y, int z)
    {
        if (x < y)
            return (x < z)? x : z;
        else
            return (y < z)? y : z;
    }

    private static int minCost(int cost[][], int m, int n)
    {
        int i, j;
        int tc[][]=new int[n+1][n+1];

        tc[0][0] = cost[0][0];

        /* Initialize first column of total cost(tc) array */
        for (i = 1; i <= n; i++)
            tc[i][0] = tc[i-1][0] + cost[i][0];

        /* Initialize first row of tc array */
        for (j = 1; j <= n; j++)
            tc[0][j] = tc[0][j-1] + cost[0][j];

        /* Construct rest of the tc array */
        for (i = 1; i <= n; i++)
            for (j = 1; j <= n; j++)
                tc[i][j] = Math.min(         tc[i-1][j],
                        tc[i][j-1]) + cost[i][j];

        return tc[m][n];
    }

//    static long findLessCost(int[][] matrix, int i, int j){
//
//
//
//    }

    public static void main(String[] args){

//        String[] strs = { "CC", "LJ", "AVZ", "BL", "MB", "LJ", "LJ", "NC", "BL"};
//
//        List<String> ss = Arrays.asList(strs);
//
//        System.out.println("" +  romanizeNames(ss));

        int cost[][]= {{1, 2, 3},
                {4, 8, 2},
                {1, 5, 3}};
        System.out.println("minimum cost to reach (2,2) = " +
                findBestRoute(cost, 3));
//                minCost(cost,2,2));
    }
}


package com.vw.datastructure.arrayandstring;

/**
 * Created by Vernon on 2/12/2016.
 * https://www.hackerrank.com/domains
 * Your teacher has given you the task of drawing a staircase structure. Being an expert programmer, you decided to make
 * a program to draw it for you instead. Given the required height, can you print a staircase as shown in the example?
 */
public class Staircase {

    public static void main(String[] args) {

        int n = 6;//Integer.valueOf(args[1]);
        for(int i = 0; i < n; i++){
            for(int j = 0; j< n; j++){
                System.out.print((j < n - ( i + 1)) ? " " : "#" );
            }
            System.out. println("");
        }

    }
}

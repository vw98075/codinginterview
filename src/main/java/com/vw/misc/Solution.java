package com.vw.misc;

import java.io.*;
import java.util.*;

/**
 * Created by Vernon on 2/14/2016.
 */
public class Solution {
    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        Scanner sc = new Scanner(System.in);
        String input = sc.next();// */ "12:40:22AM";// "07:05:45PM";
        int len = input.length();

        String amOrPm = input.substring(len - 2);
        if(amOrPm.toLowerCase().equals("am")){
            String h = input.substring(0, input.indexOf(":"));
            int hInt = Integer.valueOf(h);
            if(hInt == 12){
                System.out.println(0 + input.substring(input.indexOf(":"), input.lastIndexOf("AM")));
            }
            else
                System.out.println(input.substring(0, len - 2));
            return;
        }

        if(amOrPm.toLowerCase().equals("pm")){

            String h = input.substring(0, input.indexOf(":"));
            int hInt = Integer.valueOf(h);
            System.out.println((hInt + 12) + input.substring(input.indexOf(":"), input.lastIndexOf("PM")));
            return;
        }

        System.out.println("Time format error");


    }
}

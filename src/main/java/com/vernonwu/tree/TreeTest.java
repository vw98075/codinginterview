/*
 * Created on 2009-10-11
 *
 */
package com.vernonwu.tree;

import java.util.Iterator;

public class TreeTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Node<Integer> n1 = new Node<Integer>(1), n4 = new Node<Integer>(4), n7 = new Node<Integer>(
				7), n12 = new Node<Integer>(12),

		n3 = new Node<Integer>(3, n1, n4), n10 = new Node<Integer>(10, n7, n12), n5 = new Node<Integer>(
				5, n3, n10);

		BinaryTree<Integer> bt = new BinaryTree<Integer>(n5);

		for (Iterator<Node<Integer>> itr = bt.iterator(); itr.hasNext();) {
			System.out.println(itr.next());
		}

		Node<Integer> n = bt.findNode(4);
		System.out.println(n);
		n = bt.findNode(6);
		System.out.println(n);
		
		bt.preorderTraversal(n5);
		System.out.println();
		bt.inorderTraversal(n5);
		System.out.println();
		bt.postorderTraversal(n5);

	}

}

package com.vernonwu.tree;
/*
 * Created on 2009-10-09
 *
 */

public class Node<T> implements Comparable<T> {
	
	T value;

	Node<T> left, right;

	public Node() {
	}
	
	public Node(T t) {
		value = t;
	}

	public Node(T t, Node<T> l, Node<T> r) {
		value = t;
		left = l;
		right = r;
	}
	
	public int compareTo(Object t){
		Node<T> t1 = (Node<T>)t;
		if(this.value.hashCode() == t1.value.hashCode())
			return 0;		
		return (this.value.hashCode() < t1.value.hashCode()) ? -1 : 1;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Node<T> other = (Node<T>) obj;
		if (value == null) {
			if (other.value != null)
				return false;
		} else if (!value.equals(other.value))
			return false;
		return true;
	}

	public String toString(){

		return "value[" + (Integer.TYPE.equals(value.getClass()) ? value : value.toString()) + "]";
	}
}

package com.vernonwu.aircraftqueue;

public class Aircraft {
	
	public enum Type {Passage, Cargo};
	
	public enum Size {Large, Small};
	
	private int id;
	
	private Type type;
	
	private Size size;

	public Aircraft(int id, Type type, Size size) {
		super();
		this.id = id;
		this.type = type;
		this.size = size;
	}
	
	public int priority() {
		int d = 0b000000;
		if(type == Type.Passage)
			d += 0b000010;
		if(size == Size.Large)
			d += 0b000001;
		return d;
	}

	@Override
	public String toString() {
		return "Aircraft [id=" + id + ", type=" + type + ", size=" + size + "]";
	}	
}

package com.vernonwu.aircraftqueue;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class AircraftQueue {

	private LinkedList<Aircraft> queue;

	public AircraftQueue() {
		queue = new LinkedList<Aircraft>();
	}

	public void enqueue(Aircraft ac) {
		if (this.queue.isEmpty()) {
			queue.add(ac);
			return;
		}
		if (ac.priority() > this.queue.peekFirst().priority()) {
			this.queue.addFirst(ac);
			return;
		}
		for (int i = 0; i < this.queue.size() - 1; i++) {
			if (ac.priority() > this.queue.get(i).priority()) {
				queue.add(i, ac);
				return;
			}
		}
	}

	public Aircraft dequeue() {
		return this.queue.removeFirst();
	}

	private String printQueue() {
		StringBuilder sBuilder = new StringBuilder();
		Iterator<Aircraft> itr = queue.iterator();
		while (itr.hasNext()) {
			sBuilder.append(itr.next()).append(", ");
		}
		return sBuilder.toString();
	}

	@Override
	public String toString() {
		return "AircraftQueue [queue=" + printQueue() + "]";
	}

}

/*
 * Created on 2012-01-15
 *
 */
package com.vernonwu.firstsinglecharacter;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.Set;

public class Tester {

	static Character firstSingleCharacter(String str) {
		
		char [] chars = str.toCharArray();
		Map<Character, Integer> hm = new HashMap<Character, Integer>();
		Queue<Character> q = new LinkedList<Character>();
		for(char c : chars){
			Integer count = hm.get(c);
			if(count == null){
				hm.put(c, 1);
			}else {
				hm.put(c, count + 1);
			}
			q.add(c);			
		}
		
		while(!q.isEmpty()){
			Character c = q.remove();
			if(hm.get(c) == 1){
				return c;
			}
		}		
		return null;
	}
	
	static Character firstSingleCharacter2(String str) {
		
		char [] chars = str.toCharArray();
		Map<Character, Integer> hm = new LinkedHashMap<Character, Integer>();
		for(char c : chars){
			Integer count = hm.get(c);
			if(count == null){
				hm.put(c, 1);
			}else {
				hm.put(c, count + 1);
			}		
		}
		
		Set<Character> keySet = hm.keySet();
		for(Character c : keySet){
			if(hm.get(c) == 1){
				return c;
			}
		}		
		return null;
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {

		String str = "abcedfghijkabcdfghjk";
		System.out.println(firstSingleCharacter(str));		
		System.out.println(firstSingleCharacter2(str));
	}
}

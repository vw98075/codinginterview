/*
 * Created on Oct 7, 2011
 *
 */
package com.vernonwu.misc;

public class RemoveSpaces {

	static  void  remove_spaces1(char []array){
		String str = arrayToString(array);
		str = str.replaceAll(" ", "");
		System.out.println(str);
	}
	
    public static String arrayToString(char[] array) {
        StringBuilder arTostr = new StringBuilder();
        if (array.length > 0) {
            arTostr.append(array[0]);
            for (int i=1; i<array.length; i++) {
                arTostr.append(array[i]);
            }
        }
        return arTostr.toString();
    }

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		String str = args[0];
		char [] array = args[0].toCharArray();
		remove_spaces1(array);
	}

}

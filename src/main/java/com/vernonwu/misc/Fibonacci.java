/*
 * Created on Oct 6, 2011
 *
 */
package com.vernonwu.misc;

public class Fibonacci {

	static int fibonacci(int n) {

		if (n == 0)
			return 0;
		else if (n == 1) {
			return 1;
		} else {
			return fibonacci(n - 2) + fibonacci(n - 1);
		}
	}

	static int fibonacci2(int n) {

		if (n == 0)
			return 0;
		int a = 1, b = 1;
		for (int i = 3; i <= n; i++) {
			int c = a + b;
			a = b;
			b = c;
		}
		return b;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		int input = Integer.valueOf(args[0]);

		StringBuilder sb = new StringBuilder();
		for (int i = 0; i <= input; i++) {
			sb.append(fibonacci2(i)).append(' ');
		}
		System.out.println(sb.toString());
	}

}

/*
 * Created on Oct 6, 2011
 *
 */
package com.vernonwu.misc;

public class Palindrome {

	static boolean method1(String str){
		return (str.toLowerCase().equals((new StringBuilder(str.toLowerCase())).reverse().toString()));
	}
	
	static boolean method2(String str){
	
		char [] cs = str.toLowerCase().toCharArray();
		for(int i = 0; i < cs.length / 2; i++){
			if(cs[i] != cs[cs.length - 1 - i])
				return false;
		}
		return true;
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String input = args[0];
		
		System.out.println(method2(input) ? "yes" : "no");
		

	}

}

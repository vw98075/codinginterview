/*
 * Created on Sep 29, 2011
 *
 */
package com.vernonwu.misc;

public class PrimeTester {

	/**
	 * http://www.aaamath.com/fra63ax2.htm
	 * @param args
	 */
	public static void main(String[] args) {
		
		
		int k  = Integer.parseInt(args[0]);//.getInteger(args[0]);
		
		boolean isPrime = true;
		
		if(k == 1 || k == 0){
			isPrime = false;
		}else if(k % 2 == 0){
			isPrime = false;
		}else if(k > 5 && args[0].toCharArray()[args[0].toCharArray().length - 1] == '5'){
			isPrime = false;
		}else {
			for(int i = 3; i < k ; i++){
				if(k % i == 0){
					System.out.println("i=" + i);
					isPrime = false;		
					break;
				}
//				i = i + 2;
			}
		}
		System.out.println((isPrime ?  "The input integer is a prime. " : "The input integer is not a prime. ") + args[0]);
	}

}

package com.vernonwu.misc;

import java.util.LinkedList;

public class NumberConversation {

	 static String decimalToBinary(Integer i){
		 
		 ///2^n + 2^(n-1)  + ... 
		 
		 LinkedList<Integer> il = new LinkedList<Integer>();
		 // 11 % 2 = 1, 11 / 2 = 5, 5%2=1, 5/2=2, 2%2=0, 2/2=1,  1 % 2 = 0
		 while(i != 0){
			il.addFirst(i % 2);
			i /= 2;
		 
		 }
		 
		 StringBuilder sb = new StringBuilder();
		 for(Integer data : il){
			sb.append(String.valueOf(data));
		}
		return sb.toString();
	 }
	 
	 
	 
	public static void main(String[] args) {
		System.out.println("11 - " + decimalToBinary(11));

	}

}

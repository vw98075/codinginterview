/*
 * Created on Oct 6, 2011
 *
 */
package com.vernonwu.misc;

public class PrimeNumbers {

	static void doIt(int input){
		short counter = 0;
		StringBuilder sb = new StringBuilder();
		for(int i = 0; i <= input; i++){
			if(isPrime(i)){
				sb.append(i).append(' ');
				counter++;
			}
		}
		System.out.println(sb.toString());
		System.out.println("Total number: " + counter);
	}
	
	static boolean isPrime(int k){

		boolean isPrime = true;
		String str = String.valueOf(k);
		if(k == 1 || k == 0){
			isPrime = false;
		}else if(k % 2 == 0){
			isPrime = false;
		}else if(k > 5 && str.toCharArray()[str.toCharArray().length - 1] == '5'){
//			System.out.println("k=" + k);
			isPrime = false;
		}else {
			for(int i = 3; i < k ; i = i + 2){
				if(k % i == 0){
//					System.out.println("i=" + i);
					isPrime = false;		
					break;
				}
			}
		}
		return isPrime;		
	}
	
	public static void main(String [] args){
		
		int input = Integer.valueOf(args[0]);
		doIt(input);
		
		
	}
}

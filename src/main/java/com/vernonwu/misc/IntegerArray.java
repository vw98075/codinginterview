package com.vernonwu.misc;

import java.util.Arrays;

public class IntegerArray {

	static int[] assignValue(int[] ar, int a, int b) {
		ar[0] = a;
		ar[1] = b;
		return ar;
	}

	/*
	 * Given a sorted array (elements may or may not repeat) and number X. Find
	 * the starting and ending index of X in array. if X is not present in array
	 * return -1,-1.
	 */
	static int[] findDownAndUp(int[] sortedArray, int x) {

		int[] ar = { -1, -1 };

		if (sortedArray.length == 0)
			return ar;

		if (sortedArray.length == 1) {
			if (ar[0] == x)
				return assignValue(ar, 0, 0);

			return ar;
		}
		if (sortedArray[0] > x || sortedArray[sortedArray.length - 1] < x)
			return ar;

		int i = 0;
		while (sortedArray[i] < x)
			i++;

		int j = i;
		while (sortedArray[i] == sortedArray[j + 1]) {
			j++;
			if (j == sortedArray.length - 1)
				break;
		}

		return assignValue(ar, i, j);
	}

	public static void main(String[] args) {

		int a[] = { 1, 2, 2, 3, 3, 3, 4, 4, 4, 4, 5, 5, 5, 5, 5 };

		System.out.println("3 for " + Arrays.toString(findDownAndUp(a, 3)));
		System.out.println("4 for " + Arrays.toString(findDownAndUp(a, 4)));
		System.out.println("5 for " + Arrays.toString(findDownAndUp(a, 5)));
		System.out.println("0 for " + Arrays.toString(findDownAndUp(a, 0)));
		System.out.println("6 for " + Arrays.toString(findDownAndUp(a, 6)));
		System.out.println("9 for " + Arrays.toString(findDownAndUp(a, 9)));

	}

}

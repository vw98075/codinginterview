package com.vernonwu.integerarray;
/*
 * Created on 2009-10-08
 *
 */

public class ArrayTest {

	static void swap(Integer a, Integer b) {
		Integer tmp = a;
		a = b;
		b = tmp;
	}

	static void swap(Integer[] a, int i, int j) {
		Integer tmp = a[i];
		a[i] = a[j];
		a[j] = tmp;
	}

	static void swap(String a, String b) {
		String tmp = a;
		a = b;
		b = tmp;
	}

	static void bubbleSort(Integer[] a) {
		for (int i = a.length - 1; i > 0; i--) {

			for (int j = 1; j <= i; j++) {
				System.out.println("(i, j) = (" + String.valueOf(i) + ", "
						+ String.valueOf(j) + ")");
				if (a[j - 1].intValue() > a[j].intValue()) {
					System.out.println("swap: " + String.valueOf(a[j - 1])
							+ " with " + String.valueOf(a[j]));
					swap(a, j - 1, j);
				}
			}
		}
	}

	static Integer repeatedElement(Integer[] a) throws Exception {
		quickSort(a);

		Integer k = 0;
		boolean found = false;
		for (int i = 0; i < a.length - 1; i++) {
			if (a[i] == a[i + 1]) {
				found = true;
				k = a[i];
				break;
			}
		}
		if (!found)
			throw new Exception();
		return k;
	}

	// pre-condition: 0 <= k <= k+n <= a.length
	// post-condition: a[k] <= a[k+1] <= a[k+2] ... <= a[k+n-1]
	static void quickSortPartition(Integer[] a, int k, int n) {
		if (n < 2)
			return;

		Integer pivot = a[k];
		int i = k, j = k + n;
		while (i < j) {
			while (i + 1 < k + n && a[++i] < pivot)
				;
			while (a[--j] > pivot)
				;
			if (i < j) {
				swap(a, i, j);
				// invariant: a[p] <= pivot ,+ a[q] for k<=p <=i <=j <= q <= k+n
				System.out.println("(i, j) = (" + String.valueOf(i) + ", "
						+ String.valueOf(j) + ")");
			}
		}
		System.out.println("(k, j) = (" + String.valueOf(k) + ", "
				+ String.valueOf(j) + ")");
		swap(a, k, j);
		// invariant: a[p] <= a[j] <=a[q] for k < p < j < k+n
		quickSortPartition(a, k, j - k);
		quickSortPartition(a, j + 1, k + n - j - 1);
	}

	// +++++++++++++++++++++++++++++++++++++++++++++++++++++++
	static void quickSort(Integer[] a) {
		if (a.length > 1)
			quickSort(a, 0, a.length);
	}

	static void quickSort(Integer[] a, int p, int q) {
		if (q - p < 2)
			return;
		int m = partition(a, p, q);
		quickSort(a, p, m);
		quickSort(a, m+1, q);
	}

	private static int partition(Integer[] a, int p, int q) {
		int pivot = a[p], i = p, j = q;
		while (i < j) {
			while (i < j && a[--j] >= pivot)
				;
			if (i < j)
				a[i] = a[j];
			while (i < j && a[++i] <= pivot)
				;
			if (i < j)
				a[j] = a[i];
		}
		a[j] = pivot;
		return j;
	}
	// +++++++++++++++++++++++++++++++++++++++++++++++++++++++
	static void mergeSort(Integer[] a) {
		if (a.length > 1)
			mergeSort(a, 0, a.length);
	}

	static void mergeSort(Integer[] a, int p, int q){
		if(q-p < 2)
			return;
		int m = (p+q)/2;
		mergeSort(a, p, m);
		mergeSort(a, m, q);
		merge(a, p, m,q);
	}
	
	static void merge(Integer[] a, int p, int m, int q){
	
		// pre-condiction: a[p..m), a[m...q) sorted
		// post-condition: a[p...q) sorted
		if(a[m-1] <= a[m])
			return;
		int i=p, j=m, k=0;
		Integer[] tmp = new Integer[q-p];
		while(i<m && j<q)
			tmp[k++] = (a[i] <= a[j] ? a[i++] : a[j++]);
		System.out.println("i = " + i + ", p = " + p + ", k = " + k + ", m = " + m);
		System.arraycopy(a, i, a, p+k, m-i);
		System.arraycopy(tmp, 0, a, p, k);		
	}
	// +++++++++++++++++++++++++++++++++++++++++++++++++++++++	
	/**
	 * @param args
	 */
	public static void main(String[] args) {

		Integer[] a = { 77, 44, 99, 66, 33, 55, 88, 22 };
		print(a);
		System.out.println("before");
//		quickSort(a);
		mergeSort(a);
		System.out.println("after");
		 print(a);
		// String z = "5", y = "9";
		// System.out.println(String.valueOf(z) + " " + String.valueOf(y));
		// swap(z, y);
		// System.out.println(String.valueOf(z) + " " + String.valueOf(y));
		// try{
		// System.out.println(repeatedElement(a));
		// }catch(Exception ex){
		//			
		// }
	}

	static void print(Integer[] a) {
		for (int i = 0; i < a.length; i++)
			System.out.print(a[i] + " ");
		System.out.println();
	}
}

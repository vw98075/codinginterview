/*
 * Created on 2012-01-24
 *
 */
package com.vernonwu.lengtestprefix;

public class LengthestPrefixFindingHelper {

	String[] patters;

	public void setup(String[] p) {
		patters = p;
	}

	public String findLongestPrefix(String theInputWord) {

		int i = indexSearch(theInputWord, 0, patters.length - 1);
		return (i != -1) ? patters[i] : null;
	}

	private int indexSearch(String theInputWord, int start, int end) {

		System.out.println("Entering... Start: " + start + " end: " + end);

		if (end == start || !theInputWord.startsWith(patters[start]))
			return -1;

		if (theInputWord.startsWith(patters[end]))
			return end;

		if (theInputWord.startsWith(patters[start])
				&& !theInputWord.startsWith(patters[end]) && end - start == 1) {
			return start;
		}

		int mid = end - start / 2;
		if (theInputWord.startsWith(patters[mid])) {
			System.out.println("Up half Mid: " + mid);
			return indexSearch(theInputWord, mid + 1, end);
		} else {
			System.out.println("Low half Mid: " + mid);
			return indexSearch(theInputWord, start, mid - 1);
		}
	}

	public String findLongestPrefix2(String theInputWord) {

		int i = 0;
		while (i < patters.length - 1) {
			if (theInputWord.startsWith(patters[i])
					&& !theInputWord.startsWith(patters[i + 1]))
				break;
			i++;
		}
		if (i == patters.length - 1) {
			return (theInputWord.startsWith(patters[i])) ? patters[i] : null;
		}
		return patters[i];
	}
}

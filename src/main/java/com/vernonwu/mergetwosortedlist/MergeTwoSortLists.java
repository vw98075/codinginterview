/*
 * Created on 2012-01-15
 *
 */
package com.vernonwu.mergetwosortedlist;

public class MergeTwoSortLists {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		if(args.length < 2){
			System.err.println("Please provide two strings of integers");
			return;
		}
		
		char [] i1 = args[0].toCharArray(), i2 = args[1].toCharArray();
		int k1 = 0, k2 = 0, k3 = 0;
		char [] i3 = new char[i1.length + i2.length];
//		for(int i =0; i < i3.length; i++){
//			if(i1[k1] < i2[k2]){
//				i3[i] = i1[k1];
//				if(k1 < i1.length - 1)
//					k1++;
//			}else{
//				i3[i] =i2[k2];
//				if(k2 < i2.length - 1)
//					k2++;
//			}			
//		}
		
		while(k1 < i1.length && k2 < i2.length){
			if(i1[k1] < i2[k2]){
				i3[k3++] = i1[k1++];
			}else{
				i3[k3++] =i2[k2++];
			}				
		}
		if(k1 == i1.length){
			for(int j = k2; j < i2.length; j++){
				i3[k3++] = i2[j];
			}			
		}
		if(k2 == i2.length){
			for(int j = k1; j < i1.length; j++){
				i3[k3++] = i1[j];
			}			
		}		
		
		System.out.println("1st: " + args[0] + ", 2nd: " + args[1] + ", result: " + new String(i3) );		
		
	}

}

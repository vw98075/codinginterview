package com.vernonwu.stringstuff;

public class StringAndInteger {
	
	static int StringToInteger(String str) {
		
		char[] cs = str.toCharArray();
		int i = 0, num = 0;
		boolean isNegative = (cs[0] == '-') ? true : false;
		if(isNegative)
			i = 1;
		while(i < cs.length) {
			num *= 10;
			num += (cs[i]- '0' );
			i++;
		}
		if(isNegative)
			num = -num;		
		return num;
	}
	
	static int MAX_LENGTH = 9;
	static String IntegerToString(int num) {
		boolean isNegative =  false;
		if(num < 0) {
			num = -num;
			isNegative = true;
		}
		char[] cs = new char [MAX_LENGTH + 1];
		
		int i = 0;
		/* Fill buffer with digit characters in reverse order */
	    do {
	    	cs[i++] = (char)((num % 10) + '0');
	        num /= 10;
	    } while( num != 0 );
	    StringBuilder b = new StringBuilder();
	    if( isNegative )
	        b.append( '-' );

	    while( i > 0 ){
	        b.append( cs[--i] );
	    }
	    return b.toString();
	}
	
	public static void main(String [] args) {
		
		int num = StringToInteger("-87552"); 
		String string = IntegerToString(num);
		System.out.println("Number: " + num + ", String: " + string);
	}

}

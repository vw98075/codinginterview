/*
 * Created on Sep 29, 2011
 *
 */
package com.vernonwu.stringstuff;


public class ReverseString {

	static String reverse(String post)
	{       
	    String backward = ""; StringBuilder sBuilder = new StringBuilder();
	    for(int i = post.length()-1; i >= 0; i--) {
	        backward = backward + post.substring(i, i+1);
	        sBuilder.append(post.charAt(i));
	    }        
	    return //backward;
	    		sBuilder.toString();
	}
	
	static String reverse1(String str) {
		char [] c = str.toCharArray();
		for(int i = 0; i < (c.length - 1) / 2;  i++) {
//			swap(c[i], c[c.length - 1 - i]);
//			char tmp = c[i];
//			c[i] = c[c.length - i - 1];
//			c[c.length - i - 1] = tmp;
			swap(i, c.length - 1 - i, c);
		}
		return new String(c);
	}
	
	// not working
	static void swap(Character a, Character b){
		char tmp = a;
		a = b;
		b = tmp;		
	}

	static void swap(int i, int j, char [] cs){
		char tmp = cs[i];
		cs[i] = cs[j];
		 cs[j] = tmp;		
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		String str = "what a wonderful glorious day";
		System.out.println(str);
		System.out.println(reverse(str));
		System.out.println(reverse1(str));
//		char[] data = args[0].toCharArray();
//		
//		char tmp;
//		for(int i = 0; i < data.length / 2; i++){
////			swap(data[i], data[data.length - i - 1]);	
//			tmp = data[i];
//			data[i] = data[data.length - i - 1];
//			data[data.length - i - 1] = tmp;	
//		}
//
//		System.out.println(String.valueOf(data));	
//		for(int i=0; i< data.length; i++){
//			System.out.println(data[i]);
//		}
	}

}

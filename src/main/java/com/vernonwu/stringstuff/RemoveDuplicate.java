package com.vernonwu.stringstuff;

public class RemoveDuplicate {

	/*
	 * O(n)
	 * Space 26 or 128 O(1)
	 */
	static String removeDuplicatedCharactersWithSingleLooping(String str){
		
		if(str == null || str.length() == 1)
			return str;
			
		boolean [] ba = new boolean[26];
		for (int i = 0; i<26; i++){
			ba[i] = false;
		}
		char [] cs = str.toCharArray();
		char [] output = new char [cs.length];
		output[0] = cs[0];
		ba[cs[0] - 'a'] = true;
		int k = 1;
		for(int i=1;i<cs.length; i++){
			if(!ba[cs[i] - 'a']){
				output[k++] = cs[i];
				ba[cs[i] - 'a'] = true;
			}
		}
		return String.valueOf(output);
	}
	
	/*
	 * O(n^2)
	 */
	static String removeDuplicatedCharactersWithTwoLoopings(String str){
		
		if(str == null)
			return null;
		
		if(str.length() == 1 || str.length() == 0)
			return str;
		
		char[] c = str.toCharArray();
		int tail =1;
		for(int i =1; i<c.length; i++){
			int j ;
			for(j=0; j < tail; j++){
				System.out.println("(i, j)=(" +i + ", " + j + ")");
				if(c[i] == c[j]){
					System.out.println("Break: c[" +i + "]==c[" + j + "]");
					break;		
				}
			}
			if(j == tail){
				System.out.println("i = " +i + ", tail = " + tail);
				c[tail] = c[i];
				++tail;
			}		
		}
//		c[tail] = 0;
		String s = new String(c);
		return s.substring(0, tail);
	}

	public static void main(String[] arges) {
//		System.out.println("abcd - " + removeDuplicate("abcd"));
//		System.out.println("aaaa - " + removeDuplicate("aaaa"));
		System.out.println("aaaabbb - " + removeDuplicatedCharactersWithTwoLoopings("aaaabbb"));
//		System.out.println("ababab - " + removeDuplicate("ababab"));
		System.out.println("ceabcedagh - " + removeDuplicatedCharactersWithTwoLoopings("ceabcedagh"));
		System.out.println("ceabcedagh - " + removeDuplicatedCharactersWithSingleLooping("ceabcedagh"));
	}
}

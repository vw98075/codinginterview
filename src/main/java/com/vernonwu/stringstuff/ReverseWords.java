package com.vernonwu.stringstuff;

public class ReverseWords {

	static String reverseWords(String str) {
		String [] sa = str.split(" ");
		StringBuilder sb = new StringBuilder();
		for(int i = sa.length - 1; i >=0 ; i-- ) {
			sb.append(sa[i]).append(" ");			
		}
		return sb.toString();		
	}
	
	
	public static void main(String[] args) {
		String str = "what a wonderful glorious day";
		System.out.println(str);
		System.out.println(reverseWords(str));

	}

}

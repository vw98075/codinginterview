package com.vernonwu.recussive;

import java.util.Collection;
import java.util.List;

public class MemberPayoutUtil {

 public	static void calculatePayout(Member member) {
		double totalPayout = 0.1 * member.getMonthlyAmazonDollars() + recruiteePayout(member); 
	}

	private static double recruiteePayout(Member member) {
		
		double p = 0.0;
		for (Member m : member.getRecruitedMembers()) {
			p += 0.04 * (m.getMonthlyAmazonDollars() + recruiteePayout(m));
		}
		return p;
		
	}
	
	public static void main(String[] args) {

	}
}

package com.vernonwu.recussive;

import java.util.Collection;

public interface Member {
    public double getMonthlyAmazonDollars();
    public Collection<Member> getRecruitedMembers();
}

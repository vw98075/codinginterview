package com.vernonwu.graph.friends;

import java.util.ArrayList;
import java.util.List;

public class Member {

	String name;
	String email;
	List<Member> friends;

	public Member(String name, String email) {
		super();
		this.name = name;
		this.email = email;
		this.friends = new ArrayList<Member>();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public List<Member> getFriends() {
		return friends;
	}

	public boolean addFriend(Member member) {
		return friends.add(member);
	}
	
	public boolean removeFriend(Member member) {
		return friends.remove(member);
	}
}

package com.vernonwu.graph.friends;

public class Friend extends Member {

	public enum State {
		Unvisited, Visited, Visiting;
	}
	
	private State state;
	
	private int level;
	
	public Friend(String name, String email) {
		super(name, email);
		this.state= State.Unvisited;		
	}
	
	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public boolean isVisited() {
		return state == State.Visited;
	}
	
	public void markAsVisisted() {
		this.state = State.Visited;
	}

	@Override
	public String toString() {
		return "Friend [name=" + name
				+ ", state=" + state + ", level=" + level + " ]";
	}
	
	
}

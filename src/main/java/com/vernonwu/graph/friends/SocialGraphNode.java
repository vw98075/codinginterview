package com.vernonwu.graph.friends;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.vernonwu.graph.Graph;
import com.vernonwu.graph.Node;
import com.vernonwu.graph.Node.State;

public class SocialGraphNode {

	public static Graph createNewGraph() {
		Graph g = new Graph();
		Node[] temp = new Node[10];

		temp[0] = new Node("Alex", 2);
		temp[1] = new Node("Bob", 2);
		temp[2] = new Node("Carle", 2);
		temp[3] = new Node("Dave", 1);
		temp[4] = new Node("Elain", 1);
		temp[5] = new Node("Fale", 0);
		temp[6] = new Node("Joe", 2);
		temp[7] = new Node("Kim", 0);
		temp[8] = new Node("Gale", 0);
		temp[9] = new Node("Helen", 0);

		temp[0].addAdjacent(temp[1]);
		temp[0].addAdjacent(temp[2]);
		temp[1].addAdjacent(temp[3]);
		temp[1].addAdjacent(temp[4]);
		temp[2].addAdjacent(temp[5]);
		temp[2].addAdjacent(temp[6]);
		temp[3].addAdjacent(temp[2]);
		temp[4].addAdjacent(temp[7]);
		temp[6].addAdjacent(temp[8]);
		temp[6].addAdjacent(temp[9]);

		for (int i = 0; i < 10; i++) {
			g.addNode(temp[i]);
		}
		return g;
	}

	static void printSocialGraph(Node start) {
		LinkedList<Node> q = new LinkedList<Node>();
		Map<String, Integer> friends = new HashMap<String, Integer>();
		
		q.add(start);
		friends.put(start.getVertex(), 0);
		
		while (!q.isEmpty()) {
			Node member = q.removeFirst();
			if (member != null) {
				for (Node m : member.getAdjacent()) {
					String name = m.getVertex();
					int l = friends.get(member.getVertex());
					System.out.print("Level: " + (l + 1) + " " + name + "  ");
				
					if (!friends.containsKey(m.getVertex())) {
						q.add(m);
						friends.put(m.getVertex(), l + 1);
					}
				}
				if (member.getAdjacent().length != 0) {
					System.out.println();
				}
			}

		}
	}

	static void printSocialGraph2(FriendNode start) {
		LinkedList<FriendNode> q = new LinkedList<FriendNode>();
		start.getNode().setState(State.Visited);
		q.add(new FriendNode(start.getNode(), 0));

		while (!q.isEmpty()) {
			FriendNode node = q.removeFirst();
			if (node != null) {
				for (Node n : node.getNode().getAdjacent()) {
						System.out.print("Level: " + (node.getLevel() + 1) + " " + n.toString() + "  ");
					 if (n.getState() == Node.State.Unvisited) {
					 n.setState(Node.State.Visited);
						q.add(new FriendNode(n,  node.getLevel() + 1));
					}
				}
				if (node.getNode().getAdjacent().length != 0) {
					System.out.println();
				}
			}

		}
	}
	public static void main(String[] args) {

		Graph graph = createNewGraph();
		Node[] ns = graph.getNodes();
		printSocialGraph(ns[0]);
//		printSocialGraph2(new FriendNode(ns[0], 0));

	}

}

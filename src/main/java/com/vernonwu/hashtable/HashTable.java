package com.vernonwu.hashtable;

public class HashTable<K, V> {
	/* take the capacity as prime number to reduce the collision */

	private static int SIZE = 31;

	/* initialize array to store value */

	private V[] tableValues = (V[]) new Object[SIZE];

	public V put(K k, V v) {
		
		if (v == null) {
			throw new NullPointerException();
		}
		tableValues[hash(k.hashCode()) % SIZE] = v;
		return v;		
	}

	public V get(K k) {
		return this.tableValues[hash(k.hashCode()) % SIZE];
	}

	private synchronized int hash(int h) {
		h ^= (h >>> 20) ^ (h >>> 12);
		return h ^ (h >>> 7) ^ (h >>> 4);
	}
}

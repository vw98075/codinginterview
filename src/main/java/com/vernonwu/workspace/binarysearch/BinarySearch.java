package com.vernonwu.workspace.binarysearch;

import java.util.Arrays;

public class BinarySearch {

	 public static void binarySearch(int[ ] array, int lowerbound, int upperbound, int key)
	   {
	       int position;
	       int comparisonCount = 1;    // counting the number of comparisons (optional)

	     // To start, find the subscript of the middle position.
	     position = ( lowerbound + upperbound) / 2;

	     while((array[position] != key) && (lowerbound <= upperbound))
	     {
	         comparisonCount++;
	         if (array[position] > key)             // If the number is > key, ..
	         {                                              // decrease position by one. 
	              upperbound = position - 1;   
	         }                                                             
	              else                                                   
	        {                                                        
	              lowerbound = position + 1;    // Else, increase position by one. 
	        }
	       position = (lowerbound + upperbound) / 2;
	     }
	     if (lowerbound <= upperbound)
	     {
	           System.out.println("The number was found in array subscript" + position);
	           System.out.println("The binary search found the number after " + comparisonCount +
	                 "comparisons.");
	           // printing the number of comparisons is optional
	     }
	     else
	          System.out.println("Sorry, the number is not in this array.  The binary search made "
	                 +comparisonCount  + " comparisons.");
	  }
	static boolean bs(int[] a, int target) {
System.out.println(Arrays.toString(a));
		int len = a.length;
//		System.out.println(len/2);
		if(len == 1 || target < a[0] || target > a[len -1])
			return false;
		if (a[len / 2] == target)
			return true;

		if (a[len / 2] > target) {
			int[] a1; 
			if(len %2==0) {
				 a1 = new int[len/2] ;
				System.arraycopy(a, 0, a1, 0, len / 2);
			}else {
				 a1 = new int[len/2+1] ;
				System.arraycopy(a, 0, a1, 0, len / 2 + 1);
			}
			return bs(a1, target);
		}
//		if (a[len / 2] > target) {
		
		int[] a1 ;
			if(len % 2 == 0){
				a1 = new int[len/2] ;
				System.arraycopy(a, len/2 , a1, 0, len / 2 );
			}else {
				a1 = new int[len/2+1] ;
				System.arraycopy(a, len/2 , a1, 0, len / 2 + 1);
			}
			return bs(a1, target);
//		}
	}

	public static void main(String[] args) {
		int[] data = new int[] { 3, 4, 7, 9, 12, 14 };
		System.out.println("Looking for " + 4 + ", result: " + bs(data, 4));
		System.out.println("Looking for " + 14 + ", result: " + bs(data, 14));
		System.out.println("Looking for " + 12 + ", result: " + bs(data, 12));
		System.out.println("Looking for " + 3 + ", result: " + bs(data, 3));
		System.out.println("Looking for " + 5 + ", result: " + bs(data, 5));
//		System.out.println("Looking for " + 24 + ", result: " + bs(data, 24));
//		System.out.println("Looking for " + -4 + ", result: " + bs(data, -4));
		
		binarySearch(data, 0, 6, 4);
			binarySearch(data, 0, 6, 14);
	}

}

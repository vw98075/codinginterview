package com.vernonwu.graphicsystem;

public abstract class Graphic {

	abstract public void move(Point toPoint);
	
	abstract public void resize(double newSize);
	
	abstract public void draw();
}

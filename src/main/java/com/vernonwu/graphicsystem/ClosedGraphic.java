package com.vernonwu.graphicsystem;

abstract public class ClosedGraphic extends Graphic {

	abstract public void edgeLength();
	abstract public void calculateSize();
}

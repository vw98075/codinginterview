/*
 * Created on 2012-02-27
 *
 */
package com.vernonwu.phonekeypad;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class PhoneKeyPadToLetters {

	private static char[][] letterMap = { { 'a', 'b', 'c' }, { 'd', 'e', 'f' },
			{ 'g', 'h', 'i' }, { 'j', 'k', 'l' }, { 'm', 'n', 'o' },
			{ 'p', 'q', 'r', 's' }, { 't', 'u', 'v' }, { 'w', 'x', 'y', 'z' } };
	
	/*
	 * pre-condition of the input data: single digit integers range in 2 - 9
	 */
	private List<String> doMapping(List<Integer> digits) {

		if (digits.size() == 1) {
			List<String> letters = new ArrayList<String>(
					letterMap[digits.get(0) - 2].length);
			for (int i = 0; i < letterMap[digits.get(0) - 2].length; i++) {
				letters.add(String.valueOf(letterMap[digits.get(0) - 2][i]));
			}
			return letters;
		}
		int firstDigit = digits.remove(0);
		List<String> st = doMapping(digits);
		List<String> newList = new ArrayList<String>(letterMap[firstDigit - 2].length * st.size());
		for (String s : st) {
			for (int i = 0; i < letterMap[firstDigit - 2].length; i++)
				newList.add(String.valueOf(letterMap[firstDigit - 2][i]) + s);
		}
		return newList;
	}
	
	public List<String> findAllCharacterCombinations(List<Integer> digits){
		
		List<Integer> validInputData = new ArrayList<Integer>(digits.size());
		for(Integer c : digits){
			if(c> 1 && c < 10)
				validInputData.add(c);
		}
		return validInputData.isEmpty() ? Collections.EMPTY_LIST : this.doMapping(validInputData);
	}
}

/*
 * Created on 2012-02-27
 *
 */
package com.vernonwu.phonekeypad;

import java.util.ArrayList;
import java.util.List;

public class Tester {

	static private void printList(List<String> outputs){
		System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		System.out.println("All Possible Character Combinations: ");
		for (String s : outputs) {
			System.out.print(s + " ");
		}
		System.out.println("\nSize: " + outputs.size());
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {

		PhoneKeyPadToLetters pkptl = new PhoneKeyPadToLetters();
		
		// regular case
		List<Integer> c = new ArrayList<Integer>();
		c.add(2);
		c.add(7);
		c.add(6);
		c.add(9);
		printList(pkptl.findAllCharacterCombinations(c));		
		c.clear();		
		
		// duplicated digits
		c.add(2);
		c.add(2);
		printList(pkptl.findAllCharacterCombinations(c));		
		c.clear();
		
		// single digit outside of the range
		c.add(0);
		c.add(2);
		c.add(1);
		printList(pkptl.findAllCharacterCombinations(c));
		c.clear();
		
		// blank input
		printList(pkptl.findAllCharacterCombinations(c));
		c.clear();
		
		// single digit all outside of the range
		c.add(0);
		c.add(1);
		printList(pkptl.findAllCharacterCombinations(c));
		c.clear();
		
		// two digits outside of the range
		c.add(20);
		c.add(31);
		printList(pkptl.findAllCharacterCombinations(c));
		c.clear();
	}

}

/*
 * Created on 2012-02-27
 *
 */
package com.vernonwu.phonekeypad;

import java.util.ArrayList;
import java.util.List;

public class NumbersToCharacters {

	char[] c2 = { 'a', 'b', 'c' }, c3 = { 'd', 'e', 'f' },
			c4 = { 'g', 'h', 'i' }, c5 = { 'j', 'k', 'l' }, c6 = { 'm', 'n',
					'o' }, c7 = { 'p', 'q', 'r', 's' }, c8 = { 't', 'u', 'v' },
			c9 = { 'w', 'x', 'y', 'z' };

	static char[][] numberMap = { { 'a', 'b', 'c' }, { 'd', 'e', 'f' },
			{ 'g', 'h', 'i' }, { 'j', 'k', 'l' }, { 'm', 'n', 'o' },
			{ 'p', 'q', 'r', 's' }, { 't', 'u', 'v' }, { 'w', 'x', 'y', 'z' } };

	static List<String> getResult(List<Integer> str) {

//		List<String> out = new ArrayList<String>(str.size() * 6); // combination
//																	// numnber
//		int[] lens = new int[str.size()];
//		int k = 0;
//		for (Integer i : str) {
			// int l = numberMap[i - 2].length;
			// out.add(numberMap[i - 2]);

//			lens[k++] = numberMap[i - 2].length;
//		}
		// String tmp =

		// out.add(str.get(index))

		if (str.size() == 1) {
			List<String> s = new ArrayList<String>(
					numberMap[str.get(0) - 2].length);
			for (int j = 0; j < numberMap[str.get(0) - 2].length; j++) {
				s.add(String.valueOf(numberMap[str.get(0) - 2][j]));
			}
			return s;
		}
		int data = str.remove(0);
		List<String> st = getResult(str);
		List<String> newList = new ArrayList<String>(numberMap[data - 2].length
				+ st.size());
		for (String s : st) {
			for (int i = 0; i < numberMap[data - 2].length; i++)
				newList.add(String.valueOf(numberMap[data - 2][i]) + s);
		}
		return newList;
	}

	public static void main(String[] args) {

		if (args.length == 0) {
			System.out.println("Please enter phone numbers");
			return;
		}

		List<Integer> IntArrary = new ArrayList<Integer>(args.length);
		int k = 0;
		for (int i = 0; i < args.length; i++) {
			IntArrary.add(k++, Integer.parseInt(args[i].substring(0)));
		}
		List<String> outputs = (new PhoneKeyPadToLetters()).findAllCharacterCombinations(IntArrary);
		for (String s : outputs) {
			System.out.print(s + " ");
		}
		System.out.println("\nSize: " + outputs.size());
	}
}

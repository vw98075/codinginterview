/*
 * Created on 2012-02-04
 *
 */
package com.vernonwu.linkedlist;

import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class ReverseLinkedList {

	static void printList(List<String> l){
		
		Iterator<String> itr = l.iterator();
		StringBuilder sb = new StringBuilder();
		while(itr.hasNext()){
			sb.append(itr.next()).append(", ");
		}
		System.out.println(sb.toString());
	}
	
	static List<String> reverse(List<String> l){
		
		if(l.size() == 1){
			return l;
		}
		
		String str = l.remove(0);
		reverse(l).add(str);
		return l;
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		if (args.length == 0) {
			System.out.println("Please enter a string");
			return;
		}

		LinkedList<String> l = new LinkedList<String>();
		for(int i =0; i< args.length; i++){
			l.add(args[i]);
		}
//		System.out.println(l.toString());
//		System.out.println("The middle node: " + l.getMiddleNote());
		printList(l);
	//	reverse(l);
		Collections.reverse(l);
		printList(l);
//		System.out.println(l.toString());
//		System.out.println("The middle node: " + l.getMiddleNote());
		
		
//		List<String> l = Arrays.asList(args);
//		LinkedList<String> ll = new LinkedList<String>(l);
//		printList(ll);
//		for (int i = 0; i < ll.size(); i++) {
//			ll.addFirst(ll.removeLast());
//		}
//		printList(ll);
				

	}

}

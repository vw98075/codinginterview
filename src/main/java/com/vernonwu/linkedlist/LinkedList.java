package com.vernonwu.linkedlist;

/*
 * Created on 2009-10-09
 *
 */

public class LinkedList<T> {
	private Node<T> header;

	private int size;

	public LinkedList() {
	}

	public LinkedList(Node<T> t) {
		header = t;
		size = 1;
	}

	public boolean add(Node<T> node) {
		Node<T> last = this.getLastNode();
		if (last == null) {
			header = node;
			size++;
			return true;
		}
		last.next = node;
		size++;
		return true;
	}

	public void addFirst(Node<T> node) {
		node.next = header;
		header = node;
		size++;
	}

	public boolean add(int idx, Node<T> t) {

		if (idx == 0) {
			addFirst(t);
			return true;
		}
		if (idx > size)
			return false;
		Node<T> pointer = header;
		for (int i = 0; i < idx - 1; i++)
			pointer = pointer.next;
		t.next = pointer.next;
		pointer.next = t;
		size++;
		return true;

	}

	/*
	 * http://www.technicalypto.com/2010/01/java-program-to-reverse-singly-linked
	 * .html
	 */
	public void reverse() {
		//
		// if(header == null || header.next == null)
		// return;
		// Node<T> pointer = header.next, nextOne ;
		// while(pointer != null){
		// if(pointer.next != null)
		// nextOne = pointer.next;
		//
		//
		// }
		//
		if (this.size <= 1) {
			return;
		}
		Node<T> nearNode = this.header;
		Node<T> midNode, farNode;

		midNode = nearNode.next;
		farNode = midNode.next;

		nearNode.next = null;

		while (farNode != null) {
			midNode.next = nearNode;

			nearNode = midNode;
			midNode = farNode;
			farNode = farNode.next;
		}
		midNode.next = nearNode;
		this.header = midNode;

	}

	/*
	 * 1) the remaining portion of the original list pointed to by current, and
	 * 2) the new list pointed to by newHead. Each iteration strips the head
	 * element from the original list, and adds it to the head of the new list.
	 * http
	 * ://zerocredibility.wordpress.com/2009/10/09/reverse-a-linked-list-java/
	 */
	public void reverse2() {
		Node<T> current = this.header;
		Node<T> newHead = null;
		while (current != null) {
			Node<T> tmp = current;
			current = current.next;
			tmp.next = newHead;
			newHead = tmp;
		}
		this.header = newHead;
	}

	public Node<T> nthLast(int n) {

		if (n > size)
			return null;
		if(n == size)
			return header;
		
		int k = size - n;
		Node<T> node = header;
		for (int i = 0; i < k; i++) {
			node = node.next;
		}
		return node;
	}

	public Node<T> nthLastWithoutSize(int n) {
		if (n < 0)
			return null;
		// one node only
		// if(header.next == null)
		// return header;
		Node<T> last = header;
		int counter = 1;
		while (counter <= n && last.next != null) {
			last = last.next;
			counter++;
		}
		// shorter than the special length
		if (counter < n)
			return null;
		if (last.next == null && counter == n)
			return header;
		Node<T> nthNode = header;
		while (last != null) {
			nthNode = nthNode.next;
			last = last.next;
		}
		return nthNode;
	}

	public boolean remove(Node<T> t) {

		if (header == null)
			return false;
		if (t.equals(header)) {
			header = header.next;
			return true;
		}
		for (Node<T> pointer = header; pointer != null; pointer = pointer.next) {
			if (t.equals(pointer.next)) {
				pointer.next = pointer.next.next;
				return true;
			}
		}
		return false;
	}

	public Node<T> getLastNode() {

		if (header == null)
			return null;
		Node<T> pointer = header;
		while (pointer.next != null)
			pointer = pointer.next;
		return pointer;
	}

	public Node<T> getMiddleNote() {
		if (header == null)
			return null;
		if (this.header.next == null)
			return this.header;
		if (header.next.next == null)
			return this.header;

		Node<T> node1 = header, node2 = header.next;
		while (node2 != null) {
			node2 = node2.next;
			if (node2 == null)
				break;
			node2 = node2.next;
			node1 = node1.next;
		}
		return node1;
	}

	public Node<T> getMiddleNoteWithSize() {
		if (this.size == 1 || this.size == 2)
			return this.header;

		int middleIndex = this.size / 2;
		Node<T> middleNode = header;
		for (int i = 0; i < middleIndex; i++) {
			middleNode = middleNode.next;
		}
		return middleNode;
	}

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	public void insertInFront(T t) {
		Node<T> newNode = new Node<T>(t);
		newNode.next = header;
		header = newNode;
	}

	public void swapConsecutiveElements() {
		Node<T> runningNode = header;
		int counter = 0;
		while (runningNode != null && runningNode.next != null) {
			runningNode.next = runningNode.next.next;
			runningNode.next.next = runningNode;
			runningNode = runningNode.next;
			if (counter++ == 0)
				header = runningNode;
			if (runningNode.next.next != null)
				runningNode = runningNode.next.next;
			else
				break;
		}

	}

	public void swapEveryTwoElements() {

		Node<T> newHead = (header != null && header.next != null) ? header.next
				: header;
		Node<T> n = header;
		while (n != null && n.next != null) {
			Node<T> tmp = n; // save (1)
			n = n.next; // (1) = (2)
			tmp.next = n.next; // point to the 3rd item
			n.next = tmp; // (2) = saved (1)
			n = tmp.next; // move to item 3

			// important if there will be further swaps
			if (n != null && n.next != null)
				tmp.next = n.next;
		}

		// return the new head
		header = newHead;
	}

	void swapPair(Node n){
		
		if(n == null || n.next == null || n.next.next == null)
			return;
			
		Node<T> head = n;
		Node<T> oh = null;
		while(n.next != null && n.next.next != null){
			n.next.next = oh; // move the odd node to odd linked list
			oh = n.next;
			
			n = n.next.next; // progress 
		}
		if(n.next == null)
			n.next = oh;
		else
			n.next.next = oh;
		
	}
	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	public String toString() {
		if (header == null)
			return "";
		Node<T> pointer = header;
		StringBuilder sb = new StringBuilder("[");
		do {
			// for(int i =0; i<size; i++)
			sb.append(pointer.value).append("]->[");
			pointer = pointer.next;
		} while (pointer != null);
		sb.append("null]");
		return sb.toString();
	}
}

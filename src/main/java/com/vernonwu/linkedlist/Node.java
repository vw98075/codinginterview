package com.vernonwu.linkedlist;
/*
 * Created on 2009-10-09
 *
 */

public class Node<T> {
	
	T value;

	Node<T> next;

	public Node() {
	}
	
	public Node(T t) {
		value = t;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Node<T> other = (Node<T>) obj;
		if (value == null) {
			if (other.value != null)
				return false;
		} else if (!value.equals(other.value))
			return false;
		return true;
	}

	public String toString(){
		return "Node[value = " + value.toString() + ", next = " + ((next == null) ? "null" : next.value) + "]";
	}
}

package com.vernonwu.linkedlist;

import java.util.List;

/*
 * Created on 2009-10-09
 *
 */

public class LinkedListTest {
	

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		LinkedList<Character> l = new LinkedList<Character>();
		Node<Character> n1 = new Node<Character>('C'), n2 = new Node<Character>(
				'D'), n3 = new Node<Character>('X'), n4 = new Node<Character>(
				'Y'), n5 = new Node<Character>('Z'), n6 = new Node<Character>('Q');
		System.out.println(l.toString());
		l.add(n1);
		System.out.println(l.toString());
		l.add(n2);
		System.out.println(l.toString());
		l.add(n3);
		System.out.println(l.toString());
		l.addFirst(n4);
		System.out.println(l.toString());
		l.add(0, n5);
		System.out.println(l.toString());
		l.add(0, n6);		
		System.out.println(l.toString());	
		
		System.out.println(l.toString());
		l.swapEveryTwoElements();
		System.out.println(l.toString());
		
//		System.out.println("Middle node: " + l.getMiddleNote().toString());
//		System.out.println("Last 2th node: " + l.nthLastWithoutSize(2).toString());
//		System.out.println("Last 2th node (size): " +l.nthLast(2));
	
//		l.remove(n4);
//		System.out.println(l.toString());
//		l.remove(n5);
//		System.out.println(l.toString());
//		l.remove(n3);
//		System.out.println(l.toString());
//		l.remove(n3);
//		System.out.println(l.toString());
//		l.insertInFront('P');
//		System.out.println(l.toString());
		
	}

}

package com.vernonwu.linkedlist;

public class ReverseList2014 {

	/*
	 * What is the reverse of null (the empty list)? null. 
	 * What is the reverse of a one element list? the element. 
	 * What is the reverse of an n element list? the reverse of the second element on followed by the first element
	 */
	static public Node Reverse(Node node) {
		if (node == null)
			return null; // first question

		if (node.next == null)
			return node; // second question

		// third question - in Lisp this is easy, but we don't have cons
		// so we grab the second element (which will be the last after we
		// reverse it)

		Node secondElem = node.next;

		// bug fix - need to unlink list from the rest or you will get a cycle
		node.next = null;

		// then we reverse everything from the second element on
		Node reverseRest = Reverse(secondElem);

		// then we join the two lists
		secondElem.next = node;

		return reverseRest;
	}

	static public void recursiveReverse(Node currentNode) {
		// check for empty list
		if (currentNode == null)
			return;

		/*
		 * if we are at the TAIL node: recursive base case:
		 */
		if (currentNode.next == null) {
			// set HEAD to current TAIL since we are reversing list
//			head = currentNode;
			return; // since this is the base case
		}

		recursiveReverse(currentNode.next);
		currentNode.next.next = currentNode;
		currentNode.next = null; // set "old" next pointer to null
	}

	static public void reverseListIteratively(Node head) {
		if (head == null || head.next == null)
			return; // empty or just one node in list

		Node Second = head.next;

		// store third node before we change
		Node Third = Second.next;

		// Second's next pointer
		Second.next = head; // second now points to head
		head.next = null; // change head pointer to null

		// only two nodes, which we already reversed
		if (Third == null)
			return;

		Node CurrentNode = Third;

		Node PreviousNode = Second;

		while (CurrentNode != null) {
			Node NextNode = CurrentNode.next;

			CurrentNode.next = PreviousNode;

			/*
			 * repeat the process, but have to reset the PreviousNode and
			 * CurrentNode
			 */

			PreviousNode = CurrentNode;
			CurrentNode = NextNode;
		}

		head = PreviousNode; // reset the head node
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}

package com.vernonwu.stack;

public class StackArrayImplTest {

	public static void main(String[] args) {

		StackArrayImpl s = new StackArrayImpl();

		try {
			s.push('a');
			System.out.println(s.peep());
			System.out.println(s.toString());
			s.push('b');
			System.out.println(s.peep());
			System.out.println(s.toString());
			s.push('c');
			System.out.println(s.peep());
			System.out.println(s.toString());
			s.push('d');
			System.out.println(s.peep());
			System.out.println(s.toString());
		} catch (Exception ex) {
		}
		System.out.println("--------------------");
		System.out.println(s.pop());
		System.out.println(s.toString());
		System.out.println(s.pop());
		System.out.println(s.toString());
		System.out.println(s.pop());
		System.out.println(s.toString());
		System.out.println(s.pop());
		System.out.println(s.toString());
		System.out.println(s.pop());
		System.out.println(s.toString());
	}

}

package com.vernonwu.stack;

import java.util.Arrays;

public class StackArrayImpl {
	
	static int MAX_DEPTH = 10;
	
	private Character [] elements;
	
	private int top_index;
	
	public StackArrayImpl() {
		elements = new Character[MAX_DEPTH];
		this.top_index = -1;
	}
	
	public void push(Character c) throws Exception{
		if(this.top_index == this.MAX_DEPTH - 1)
			throw new Exception("");
		
		elements[++top_index] = c;		
	}
	
	public Character peep() {
		return elements[top_index];
	}
	
	public Character pop() {
		if(top_index == -1)
			return null;
		
		return elements[top_index--];
	}

	@Override
	public String toString() {
		return "StackArrayImpl [elements=" + Arrays.toString(elements)
				+ ", top_index=" + top_index + "]";
	}
}

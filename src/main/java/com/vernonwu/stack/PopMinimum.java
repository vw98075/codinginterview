package com.vernonwu.stack;

import java.util.Stack;

public class PopMinimum {

	private Stack<Integer> stack = new Stack<Integer>(),
			minStack = new Stack<Integer>(), utilStack = new Stack<Integer>();

	public void push(Integer d) {
		stack.push(d);
		if (minStack.size() == 0) {
			minStack.push(d);
			return;
		}
		try {

			Integer t = minStack.peek();
			if (d < t) {
				minStack.push(d);
			} else {
				Integer a;
				do {
					a = minStack.pop();
					utilStack.push(a);
				} while (d > a && minStack.size() > 0);

				minStack.add(d);

				for (int i = 0; i < utilStack.size(); i++) {
					minStack.push(utilStack.pop());
				}
			}
		} catch (Exception ex) {
			System.err.println(ex);
		}
	}

	public Integer pop() throws Exception {

		Integer d = minStack.pop();
		Integer r;
		do {
			r = stack.pop();
		} while (r > d);

		return r;
	}

	public int size() {
		return stack.size();
	}
}

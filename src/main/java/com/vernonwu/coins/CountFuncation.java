package com.vernonwu.coins;

public class CountFuncation {

	static NumberOfCoins countIt(int input, NumberOfCoins supply)
			throws Exception {

		if (input < 0)
			throw new Exception("Invalid input data");

		NumberOfCoins noc = new NumberOfCoins();
		if (supply == null) {
			int num = input / 25;
			noc.setQuarter(num);

			int remain = input % 25;
			num = remain / 10;
			noc.setDime(num);

			remain = remain % 10;
			num = remain / 5;
			noc.setNickel(num);

			remain = remain % 5;
			noc.setPenny(remain);

			return noc;
		}

		int num = input / 25;
		int remain = input % 25;
		remain = (num <= supply.getQuarter()) ? remain : remain
				+ (num - supply.getQuarter()) * 25;
		num = (num <= supply.getQuarter()) ? num : supply.getQuarter();
		noc.setQuarter(num);

		num = remain / 10;
		remain = remain % 10;
		remain = (num <= supply.getDime()) ? remain : remain
				+ (num - supply.getDime()) * 10;
		num = (num <= supply.getDime()) ? num : supply.getDime();
		noc.setDime(num);

		num = remain / 5;
		remain = remain % 5;
		remain = (num <= supply.getNickel()) ? remain : (num - supply
				.getNickel()) * 5;
		num = (num <= supply.getNickel()) ? num : supply.getNickel();
		noc.setNickel(num);

		if (remain > supply.getPenny())
			throw new Exception("Overflaw on penny");

		noc.setPenny(remain);

		return noc;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		try {
			System.out.println(countIt(99, new NumberOfCoins(1, 1, 99, 99)).toString());
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
	}

}

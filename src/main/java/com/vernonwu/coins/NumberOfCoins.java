package com.vernonwu.coins;

public class NumberOfCoins {

	private int quarter;
	private int dime;
	private int nickel;
	private int penny;
	
	public NumberOfCoins(){}
	
	public NumberOfCoins(int quarter, int dime, int nickel, int penny) {
		this.quarter = quarter;
		this.dime = dime;
		this.nickel = nickel;
		this.penny = penny;
	}

	public int getQuarter() {
		return quarter;
	}

	public void setQuarter(int quarter) {
		this.quarter = quarter;
	}

	public int getDime() {
		return dime;
	}

	public void setDime(int dime) {
		this.dime = dime;
	}

	public int getNickel() {
		return nickel;
	}

	public void setNickel(int nickel) {
		this.nickel = nickel;
	}

	public int getPenny() {
		return penny;
	}

	public void setPenny(int penny) {
		this.penny = penny;
	}

	@Override
	public String toString() {
		return "NumberOfCoins [quarter=" + quarter + ", dime=" + dime
				+ ", nickel=" + nickel + ", penny=" + penny + "]";
	}
}

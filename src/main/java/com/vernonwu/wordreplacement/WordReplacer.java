package com.vernonwu.wordreplacement;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;

/*
 * This class lets you determine how many times a particular word occurs in a file
 * and allows replacing all occurrences of a word with another word.
 *
 * For the purposes of this class, a "word" is any non-empty sequence of the letters
 * 'A' through 'Z', including both upper and lowercase.  Words are case sensitive,
 * meaning two words with the same sequence of letters are not the same if the
 * capitalization differs between them.
 *
 * When implementing this class, you are free to add additional methods and
 * instance variables, but the signatures of the existing methods should not
 * be modified except to allow throwing exceptions.
 * It is safe to assume file sizes are small enough to fit into memory.
 * For any questions about functionality that are not specified here, 
 * do whatever you feel is reasonable.
 */
public class WordReplacer {

	private String text;

	public WordReplacer(File textFile) {

		text = this.readFileToString(textFile);
	}

	private String readFileToString(File textFile) {
		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(textFile));
			StringBuilder sb = new StringBuilder();
			String line = br.readLine();

			while (line != null) {
				sb.append(line);
				sb.append("\n");
				line = br.readLine();
			}
			return sb.toString();
		} catch (Exception ex) {
			ex.printStackTrace();
			return null;
		} finally {
			try {
				br.close();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
	}

	public int getCountOfOccurrences(String word) {
		if(this.text == null)
			return 0;
		
		int counter = 0;
		String[] list = text.split("[ \n\t\r:;',.(){}]");
		for (int i = 0; i < list.length; i++) {
			if (list[i].equals(word)) {
				counter++;
			}
		}
		return counter;
	}

	public int replaceAllOccurrences(String originalWord, String newWord) {
		if(this.text == null)
			return 0;
		
		int counter = getCountOfOccurrences(originalWord);
		text = text.replaceAll(originalWord, newWord);
		return counter;
	}

	public void writeUpdatedTextToFile(File file) {
		if(this.text == null)
			return;
		
		FileOutputStream fop = null;
		try {			
			fop = new FileOutputStream(file);
			if (!file.exists()) {
				file.createNewFile();
			} 
			
			byte[] contentInBytes = text.getBytes();
 
			fop.write(contentInBytes);
			fop.flush();
			fop.close();
 
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (fop != null) {
					fop.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public String toString() {
		return "WordReplacer [text=" + text + "]";
	}
}

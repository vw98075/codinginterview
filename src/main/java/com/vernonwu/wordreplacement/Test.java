package com.vernonwu.wordreplacement;

import java.io.File;

public class Test {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		WordReplacer wr = new WordReplacer(new File("C:\\text_sample.txt"));
		System.out.println( wr.toString());
		System.out.println( wr.getCountOfOccurrences("2014"));
		System.out.println( wr.replaceAllOccurrences("2014", "2015"));
		System.out.println( wr.toString());
		File outputFile = new File("text_sample_output.txt");
		wr.writeUpdatedTextToFile(outputFile);
	}

}

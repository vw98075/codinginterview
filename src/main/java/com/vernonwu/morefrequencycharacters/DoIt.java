package com.vernonwu.morefrequencycharacters;

import java.util.HashMap;
import java.util.Map;

public class DoIt {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		String str = "what a wonderful glorious day";
		Map<Character, Integer> cs = new HashMap<Character, Integer>();
		int highestCount = 0;
		Character highestKey = null;
		for (int i = 0; i < str.length(); i++) {
			if (str.charAt(i) != ' ') {
				// System.out.println(highestKey + ' ' + highestCount);
				Integer counter = cs.get(str.charAt(i));
				if (counter == null) {
					System.out.println("A new char: " + str.charAt(i));
					cs.put(str.charAt(i), 1);
				} else {
					System.out.println("A updated char: " + str.charAt(i)
							+ " - " + (counter + 1));
					cs.put(str.charAt(i), counter + 1);
					if (counter + 1 > highestCount) {
						highestCount++;
						highestKey = str.charAt(i);
						System.out.println("highest: " + str.charAt(i));
					}
				}
			}
		}
		System.out.println("result: " + highestKey + " count: " + highestCount);

	}

}

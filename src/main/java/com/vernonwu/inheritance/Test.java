/*
 * Created on 2012-01-10
 *
 */
package com.vernonwu.inheritance;

public class Test {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		/*
		 * 1. Can a static method be override? - yes
		 * 2. @Override annotation usage
		 */
//		A a = new A();
//		A b = new B();
//		
//		A.method1();
//		B.method1();
//		b.method2();
//		((A)b).method2();
		
		/*
		 * pass by value or object? - value or copy of object reference
		 */
		A a1 = new A(1), a2 = new A(2);
//		System.out.println("Two entities: " + a1.toString() + ", " + a2.toString());
//		A.swap(a1, a2);
//		System.out.println("Two entities: " + a1.toString() + ", " + a2.toString());
		System.out.println("Value: " + a1.toString());
		A.increaseValueByFive(a1);
		System.out.println("Value: " + a1.toString());
	}

}

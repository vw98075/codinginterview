/*
 * Created on 2012-01-10
 *
 */
package com.vernonwu.inheritance;

public class A {

	private Integer value;

	A() {

	}

	A(Integer i) {
		this.value = i;
	}

	void setValue(Integer i){
		this.value = i;
	}
	
	Integer getValue(){
		return this.value;
	}
	
	void increaseValueByTen(){
		this.value = this.value + 10;
	}

	static void increaseValueByFive(A a){
		a = new A(a.value + 10);
	}
	
	static void method1() {
		System.out.println("A:method1");
	}

	void method2() {
		System.out.println("A:method2");
	}

	static void swap(A a1, A a2) {
		Integer tmp = a1.value;
		a1 = a2;
		a2.setValue(tmp);		
	}
	
	@Override
	public String toString(){
		return String.valueOf(this.value);
	}
}

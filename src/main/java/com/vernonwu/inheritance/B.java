/*
 * Created on 2012-01-10
 *
 */
package com.vernonwu.inheritance;

public class B extends A {

//	@Override
	static void method1(){
		System.out.println("B:method1");
	}
	
	@Override
	void method2(){
		System.out.println("B:method2");
	}
}

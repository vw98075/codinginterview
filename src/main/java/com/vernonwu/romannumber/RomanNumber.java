package com.vernonwu.romannumber;

public class RomanNumber {

	// Set up key numerals and numeral pairs
	static int[] values = new int[] { 1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5,
			4, 1 };
	static String[] numerals = new String[] { "M", "CM", "D", "CD", "C", "XC", "L",
			"XL", "X", "IX", "V", "IV", "I" };

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		int number = 367;
		// Initialise the string builder
		StringBuilder result = new StringBuilder();
		
		// Loop through each of the values to diminish the number
		for (int i = 0; i < 13; i++)
		{
		    // If the number being converted is less than the test value, append
		    // the corresponding numeral or numeral pair to the resultant string
		    while (number >= values[i])
		    {
		        number -= values[i];
		        result.append(numerals[i]);
		    }
		}
		System.out.println(result.toString());
	}

}
